package joinme;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class gui extends JApplet implements ActionListener {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init() {
		// set flow layout for the frame
		this.getContentPane().setLayout(new FlowLayout());
		 setLayout(new FlowLayout());
		 
		   JLabel label1 = new JLabel("URL to share: x.x.x.x.x", JLabel.RIGHT);
	    
		    getContentPane().add(label1);
		    
		JButton button1 = new JButton();
		button1.setText("1. Get data to record      ");

		JButton button2 = new JButton();
		button2.setText("2. Make space available");
		
		JButton button3 = new JButton();
		button3.setText("3. Start recording            ");
		
		JButton startButton = new JButton();
		startButton.setText("Stop");
		// add buttons to frame
		add(button1);
        //Add action listener to button
        button1.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
            	button1.setEnabled(false);
                //Execute when button is pressed
            	main.main(null);
            }
        });  
        
        
		add(button2);
	
		button2.addActionListener(new ActionListener() {
			 
            public void actionPerformed(ActionEvent ae)
            {
            	button2.setEnabled(false);
                //Execute when button is pressed
            	delete.main(null);
            }
        });  

	add(button3);
	button3.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e)
        {
            //Execute when button is pressed
        	try {
				recording.main(null);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }); }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}}