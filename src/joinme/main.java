package joinme;

import joinme.recording; 
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
 
public class main {
	 
    public static void main(String args[]){
    	//Making screenshot to save it and upload it to the ftp server of us
        //Displaying current time
 
        //Creating timer which executes once after five seconds
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new test(), 1, 1);
    }
}
    class test extends TimerTask{
    	public void run(){
    		SimpleDateFormat ft = 
    	new SimpleDateFormat ("hh-mm-ss");
            Date today2 = new Date( );
            
        String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
        String host = "HOST";
        String user = "FTP USER HERE";
        String pass = "FTP PASSWORD HERE";
        String filePath = "YOUR FOLDER WHERE YOU WANT TO STORE THE IMAGES" + ft.format(today2) + ".jpg";

     // Get the date today using Calendar object.
     // Using DateFormat format method we can create a string 
     // representation of a date with the defined format.
     String uploadPath = ("DOMAIN PATH" + ft.format(today2) + ".jpg");
     
 
        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        System.out.println("Upload URL: " + ftpUrl);
 
        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);
 
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
 
            inputStream.close();
            outputStream.close();
 
            System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    }