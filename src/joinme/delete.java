package joinme;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.net.ftp.FTPClient;

public class delete {
 public static void main(String args[]) {
	 
     //Creating timer which executes once after five seconds
     Timer timer = new Timer();
     timer.scheduleAtFixedRate(new test(), 5000, 8000);

  // get an ftpClient object
  FTPClient ftpClient = new FTPClient();
  @SuppressWarnings("unused")
class test extends TimerTask{
	public void run(){
		SimpleDateFormat ft = 
	new SimpleDateFormat ("hh-mm-ss");
        Date today2 = new Date( );
        
  try {
   // pass directory path on server to connect
   ftpClient.connect("192.168.1.38");

   // pass username and password, returned true if authentication is
   // successful
   boolean login = ftpClient.login("test", "t");

   if (login) {
    System.out.println("Connection established...");

    boolean delete = ftpClient.deleteFile("" + ft.format(today2) + ".jpg");
    if (delete) {
     System.out.println("File deleted successfully !");
    } else {
     System.out.println("Error in deleting file !");
    }

    // logout the user, returned true if logout successfully
    boolean logout = ftpClient.logout();
    if (logout) {
     System.out.println("Connection close...");
    }
   } else {
    System.out.println("Connection fail...");
   }

  } catch (SocketException e) {
   e.printStackTrace();
  } catch (IOException e) {
   e.printStackTrace();
  } finally {
   try {
    ftpClient.disconnect();
   } catch (IOException e) {
    e.printStackTrace();
   }
  }
 }
}
 }
}